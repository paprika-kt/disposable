package ca.jsbr.disposable

import kotlin.test.Test
import kotlin.test.assertTrue

class ActionDisposableTest {

    @Test
    fun shouldCallActionFunctionOnDispose() {
        var called = false
        val fixture = ActionDisposable { called = !called }
        fixture.dispose()
        assertTrue(called)
    }

    @Test
    fun shouldCallActionFunctionOnlyOnceOnMultipleDispose() {
        var called = false
        val fixture = ActionDisposable { called = !called }
        fixture.dispose()
        fixture.dispose()
        assertTrue(called)
    }


}