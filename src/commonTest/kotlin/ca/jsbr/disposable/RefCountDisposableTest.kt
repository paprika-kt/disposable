package ca.jsbr.disposable

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class RefCountDisposableTest {

    @Test
    fun shouldBeDisposedIfRefCountDisposableIsDispose(){
        var called = false
        val fixture = RefCountDisposable { called = !called }
        fixture.dispose()
        assertTrue(called)
    }

    @Test
    fun shouldBeDisposedIfRefCountDisposableIIsDisposeAndHasRefDisposableNotDisposed(){
        var called = false
        val fixture = RefCountDisposable { called = !called }
        fixture.getDisposable()
        fixture.dispose()
        assertTrue(called)
    }

    @Test
    fun shouldBeDisposedIfTheOnlyRefDisposableIsDisposed(){
        var called = false
        val fixture = RefCountDisposable { called = !called }
        fixture.getDisposable().dispose()
        assertTrue(called)
    }

    @Test
    fun shouldBeDisposedIfTheAllRefDisposableIsDisposed(){
        var called = false
        val fixture = RefCountDisposable { called = !called }
        val d = fixture.getDisposable()
        fixture.getDisposable().dispose()
        assertFalse(called)
        d.dispose()
        assertTrue(called)
    }
}