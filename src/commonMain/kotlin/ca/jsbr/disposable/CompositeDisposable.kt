package ca.jsbr.disposable

/**
 * A disposable container that can hold onto multiple other disposables
 */
open class CompositeDisposable(private val disposables: ArrayList<Disposable>) : Disposable {
    var isDisposed = false
        private set

    constructor() : this(ArrayList())

    fun add(vararg disposables: Disposable) {
        if (isDisposed) {
            return disposables.forEach { it.dispose() }
        }
        this.disposables.addAll(disposables)
    }

    fun add(disposable: () -> Unit) {
        if (isDisposed) {
            return disposable()
        }
        val d = ActionDisposable(disposable)
        this.disposables.add(d)
    }

    fun remove(disposable: Disposable) {
        this.disposables.remove(disposable)
    }

    override fun dispose() {
        if (isDisposed) return
        isDisposed = true
        this.disposables.forEach { it.dispose() }
        this.disposables.clear()
    }

}