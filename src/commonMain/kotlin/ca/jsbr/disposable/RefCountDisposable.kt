package ca.jsbr.disposable

/**
 * Represents a disposable that only disposes its underlying disposable when all dependent disposables
 * have been disposed.
 * @see getDisposable
 *
 * @param disposable Underlying disposable.
 */
open class RefCountDisposable(private val disposable: Disposable) : Disposable {
    constructor(action: () -> Unit) : this(ActionDisposable(action))

    var isDisposed = false
        private set

    private var counter = 0

    /**
     * Returns a dependent disposable that when disposed decreases the refcount on the underlying disposable.
     */
    fun getDisposable(): Disposable {
        if (isDisposed)
            return Disposable.empty
        counter++
        return ActionDisposable {
            if (--counter <= 0)
                disposable.dispose()
        }
    }

    override fun dispose() {
        counter = 0
        if (!isDisposed)
            disposable.dispose()
        isDisposed = true
    }
}