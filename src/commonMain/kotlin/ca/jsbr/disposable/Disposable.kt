package ca.jsbr.disposable

interface Disposable {

    companion object {
        val empty: Disposable = ActionDisposable {}
        fun create(action: () -> Unit): Disposable = ActionDisposable(action)
    }

    fun dispose()
}