package ca.jsbr.disposable


/**
 * Basic Disposable, call action on dispose.
 * @param action
 */
open class ActionDisposable(private val action: () -> Unit) : Disposable {
    var isDisposed = false
        private set

    override fun dispose() {
        if (!isDisposed)
            action()
        isDisposed = true
    }
}