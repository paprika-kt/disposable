
plugins {
    kotlin("multiplatform")
}

version = "unspecified"
{}
repositories {
    mavenCentral()
}

kotlin {
    jvm()
    js() {
    }
    iosX64("ios") {
        binaries {
            framework()
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }

        val jvmMain by getting {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
            }
        }

        val jvmTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }

        val jsMain by getting {
            dependencies {
                implementation(kotlin("stdlib-js"))
            }
        }

        val jsTest by getting {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }

        val iosMain by getting {
            dependencies {
            }
        }
    }
}

//Try to make kotlin test work
val compileTestJs = tasks.getByName("compileKotlinJs")
tasks.getByName("compileTestKotlinJs").doLast {
    val scripts = arrayListOf<String>()
    (compileTestJs.outputs.files + outputs.files + configurations["jsRuntimeClasspath"] + configurations["jsTestRuntimeClasspath"]).forEach { file ->
        if (file.isFile)
            copy {
                includeEmptyDirs = false

                from(if (file.extension == "js") file else zipTree(file.absolutePath))
                into("$projectDir/build/test/js/")
                include { fileTreeElement ->
                    val path = fileTreeElement.path
                    if (path.endsWith(".js") && !path.endsWith("meta.js"))
                        scripts.add("js/$path")
                    (path.endsWith(".js") || path.endsWith(".map")) && (path.startsWith("META-INF/resources/") || !path.startsWith("META-INF/"))
                }
            }
    }

    copy {
        from("src/jsTest/resources/test.html")
        into("build/test/")
        filter {
            it.replace("@@script@@", scripts.joinToString("") { "\n\t<script src=\"$it\"></script>" })
        }
    }

}