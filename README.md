Disposable
==========

Provides a set of class for creating Disposables, which defines a method to release allocated resources.
Inspered by rx.net disposable:
https://docs.microsoft.com/en-us/previous-versions/dotnet/reactive-extensions/hh229090(v=vs.103)


ActionDisposable
-----------------

```kotlin
val disposable = Disposable.create { /*do something*/ })
diposable.dispose()
//or
val disposable = ActionDisposable { /*do something*/ })
disposable.dispose()

```

CompositeDisposable
-------------------

A disposable container that can hold onto multiple other disposables

```kotlin
val compositeDisposable = CompositeDisposable(arrayListOf(Disposable.create { /*do something*/ }))
compositeDisposable.add { /*do something*/ }
compositeDisposable.add(Disposable.create { /*do something*/ })
compositeDisposable.dispose()

```


SerialDisposable
----------------

A Disposable container that allows atomically updating/replacing the contained
Disposable with another Disposable, disposing the old one when updating plus
handling the disposition when the container itself is disposed.

 
```kotlin
val disposable = SerialDisposable(arrayListOf(Disposable.create { /*do something*/ }))
disposable.disposable = ActionDisposable { /*do something*/ }
disposable.disposable = ActionDisposable { /*do something*/ }
disposable.dispose()

```

RefCountDisposable
-------------------

Represents a disposable that only disposes its underlying disposable when all dependent disposables have been disposed.

```kotlin
val refCountDisposable = RefCountDisposable { /*do something*/ }
val disposable1 = refCountDisposable.getDisposable()
val disposable2 = refCountDisposable.getDisposable()
disposable1.dispose()
disposable2.dispose()

```